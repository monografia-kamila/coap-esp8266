/*
 * Client CoAP Temperature & Humidity Sensor
 * ESP8266 ESP-12, DHT11
 * Author: kamilardg@gmail.com
 */
 
#include <Adafruit_Sensor.h> // - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor
#include <DHT.h> // - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
#include <DHT_U.h>

#include <ESP8266WiFi.h> // - ESP-COAP Client: https://github.com/automote/ESP-CoAP
#include "coap_client.h"

#define DHTPIN D1  // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

/* Instance for coapclient */
coapClient coap;

/* WiFi connection info */
const char* ssid = "ALHN-0418";
const char* password = "0110176246";

/* IP address and default port of coap server */
IPAddress ip(192, 168, 1, 70);
int port = 5683;

/* Coap client response callback */
void callback_response(coapPacket &packet, IPAddress ip, int port);

/* Coap client response callback */
void callback_response(coapPacket &packet, IPAddress ip, int port) {
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;

  /* Response from coap server */
  if (packet.type == 3 && packet.code == 0) {
    Serial.println("Ping ok");
  }
  Serial.println(p);
}


void setup() {

  Serial.begin(115200);
  dht.begin();

  /* Connection info to WiFi network */
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    //delay(500);
    yield();
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  /* Print the IP address of client */
  Serial.println(WiFi.localIP());

  /* Client response callback */
  coap.response(callback_response);

  /* Start coap client */
  coap.start();

}

void loop() {

  /* Get temperature */
  float t = dht.readTemperature();
  if (isnan(t)) {
    Serial.println(F("Error reading temperature!"));
    return;
  }
  
  /* Get humidity */
  float h = dht.readHumidity();
  if (isnan(h)) {
    Serial.println(F("Error reading humidity!"));
  }

  /*------------------------------------------------------------*/
  /* Converting float to char */
  int parte_inteira_t = t;  
  float parte_decimal_t = (t - parte_inteira_t) * 100;
  int decimal_t = static_cast<int>(parte_decimal_t);

  int parte_inteira_h = h;  
  float parte_decimal_h = (h - parte_inteira_h) * 100;
  
  char str[100];
  sprintf(str, "%d.%d/%d", parte_inteira_t, decimal_t, parte_inteira_h);
  Serial.println(str);
  
  /*------------------------------------------------------------*/
  /**
   * Request POST to send temperature and humidity:
   * post(IPAddress ip, int port, char *url, char *payload,int payloadlen);
   */
  int msgid = coap.post(ip, port, "temperatura", str, strlen("00.00/00"));
  Serial.print("Msg id: ");
  Serial.println(msgid);
  delay(60000);

}
